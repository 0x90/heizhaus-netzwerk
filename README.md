# Heizhaus Netzwerk

## Zugangsdaten

### QR Code

![QR Code](QRcode-AP-PaketQUELLE.png)

### PDF

Eine PDF mit den Zugangsdaten als QR Code und Klartextzum liegt zum Ausdrucken bei.

[Link zur PDF](https://git.0x90.space/0x90/heizhaus-netzwerk/src/branch/master/QRcode-AP-PaketQUELLE.pdf)

## Netzplan

![Heizhaus Netzwerk](heizhaus_netzwerk.png)

## Weitere Infos

* Das Diagram ist mit [PlantUML](https://plantuml.com/) erstellt.
* Es gibt auch einen [Online-Editor](https://plantuml-editor.kkeisuke.com/).
